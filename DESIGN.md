# Design Choices
## First Sprint
The first sprint required `spawn()` and `destroy()` methods to be implemented including JUnit tests to ensure that the methods are working correctly.

### spawn()
Within the `spawn()` method a decision was made to pass an `ArrayList<String>` argument into the `ProcessBuilder` object. 
This provided the simplest way to pass arguments into the executable that was required to be spawned.

### destroy()
The `Process` class in Java already has a destroy class. This method was used to create the `destroy()` method within the `ProcessManager` class.

### isRunning()
To ensure that a process is running, this method was created as a helper method.
This method becomes useful when testing the `ProcessManager` class to find out if a process has started and also if it has been destroyed.

***

### Relevant JUnit tests
#### spawnTest()
This test will create a new `ProcessManager` object `p` and also pass the arguments `notepad` for the executable and `COMP255` as the arguments.
Using the helper method `isRunning()` of `ProcessManager`, if that method returns true, then the test passes.

#### destroyTest()
This test will begin with the same test of `spawnTest()`, but then waits for 1.5 seconds to ensure that visually the executable is running.
Once that time has passed, the process is then destroyed. `isRunning()` is called again in the test to find out if the process has been destroyed.
If the process has been destroyed (`isRunning()` returns false), then the test passes.

***

## Second Sprint
The second sprint required `spawnAndCollect()` method to be implemented including JUnit tests to ensure that the method is working correctly.

### spawnAndCollect()
Within the `spawnAndCollect()` method it uses the same code as another method in the class `spawn()`.
The differences arise afterwards where the code will get the input stream from the process created.
This essentially means that it gets the output from the process that it spawned. 
The `InputStream` from the process is concatenated into one string including newline (`\n`) characters in relevant positions.
This string is then returned as required by the method.

***

### Relevant JUnit tests
#### spawnAndCollectTest()
This test will create a new `ProcessManager` object `p` and also pass the arguments `tasklist` for the executable and `/?` as the argument.
The first test will check to see if the output from the process contains a description of tasklist "`Description: This tool displays a list of currently running processes on either a local or remote machine.`".
If the test is true, then the method passes part of the tests.

The test will then create another `ProcessManager` object of `p2` and also pass the arguments `tasklist` for the executable and nothing as the argument.
The first test will check to see if the output returns null, if so the test will fail.
The second test will check to see if the output contains `System Idle Process` which is a constantly running process in Microsoft Windows.
If the test is true, then the method will pass part of the tests.
A third test was added with a helper executable that was developed called `InAndOut.exe`. The helper executable will return the arguments given to it to `stdout`.
The test passes if the output from the method matches the arguments passed to the executable.

##### Possible Changes to JUnit tests
This is mostly relevant to the `spawnAndCollectTest()` method. 
The test could employ the use of a program that will run on any machine that runs Java instead of just a Microsoft Windows machine.
This "helper" Java program could be placed in the root directory of the `ProcessManager` class and take arguments and then also return an output to standard output.
This is not in place at this point in time since the requirements do not specify the operating system required. It can be added in the future to improve usability between different operating systems.

***

## Third Sprint 
The third sprint required the `spawnAndCollectWithTimeout()` method to be implemented including JUnit tests to ensure that the method is working correctly.

### spawnAndCollectWithTimeout()
This method has an integer value passed into it and also uses code from the other method `spawnAndCollect()`. The additions that were made is that
the program will keep running until either the spawned process ends and returns a value to `stdout` or that the timeout occurs. Whichever comes first
will end the method. When the process closes and returns its values, the method returns the process's output. On the other hand, if the process does
not respond by the timeout, then the program will throw a `TimeoutException`.

***

### Relevant JUnit tests

#### InAndOutWait.exe
This executable was developed to assist with creating JUnit tests that test what is expected from the method `spawnAndCollectWithTimeout()`. It takes an argument,
waits two seconds, and then returns the argument passed to `stdout`. This enables the JUnit tests to have timeouts greater than and less than how long
`InAndOutWait.exe` takes.

#### spawnAndCollectWithTimeoutTest()
This test will create a new `ProcessManager` object `p` and also pass the argument `InAndOutWait.exe` for the executable that is spawns and `hello` as the argument.
`InAndOutWait.exe` will take an input, wait two seconds and then return the argument as an output to `stdout`. This test has a timeout of 2.1 seconds, meaning that
the test will pass if the output from the method `spawnAndCollectWithTimeout()` matches the input `hello`.

#### spawnAndCollectWithTimeoutException()
This test will create a new `ProcessManager` object `p` and also pass the argument `InAndOutWait.exe` for the executable that is spawns and `hello` as the argument.
`InAndOutWait.exe` will take an input, wait two seconds and then return the argument as an output to `stdout`. This test has a timeout of 1 seconds, meaning that
the test will pass if the expected exception of `TimeoutException` is thrown.

***

## Fourth Sprint
The fourth sprint required the `send()` and `expect()` methods to be implemented and JUnit tests relevant to these methods that ensure that the methods work correctly.

### send()
This method will send a string to an already running process that is managed by ProcessManager. The method will return true if it was able to send a string to the
process. Otherwise, if it was unable to, an exception will occur and the method will return false.

### expect()
This method will have two variables passed to it. The former being a prompt that is expected to occur from a spawned process and the latter being a timeout
for the process to send the prompt to the standard output. The code uses a scanner that will check for any inputs, and also check if the input matches the
expected prompt. If the prompt matches, then the method will return that prompt as a string. Otherwise the method will return an empty string.

Unfortunately the scanner used in the `expect()` method currently does not read the prompt from any test process. This can be rectified in future sprints.

***

### Relevant JUnit tests

#### sendTest()
This test spawns a new process of `test.bat`. This batch script will prompt the user for an input and wait for that to occur.
Once an input has been read by the script, the script will then print that input and end execution.
The test will then check to see if the script is still running, and if it is not then the test will pass.

#### expectTest()
This test spawns a new process of `test.bat`. This batch script will prompt the user for an input and wait for that to occur.
The test will then check to see if the returned string of `expect()` matches the actual expected prompt from the source code
of test.bat.

***

## Fifth Sprint
The fifth sprint required the `expect()` and `send()` methods to be extended so that it is able to be used many times in an instance of ProcessManager. JUnit tests were made to ensure that the methods work as intended.

### send()
This method has been extended from the previous sprint. It now closes the processWriter stream after it write() has been called on it. 
This change was made due to a conflict with the `expect()` method as the processWriter would have a conflict with bufferedReader in `expect()`.
The change resolved an issue where `expect()` could not be called multiple time on the same ProcessManager process.
It will also try to close the stream again if an exception is raised.

### expect()
This method has been extended from the previous sprint. It will now continuously check the inputStream of the process to see if any of
the outputs from the process match the expected prompt. Once the output from the process matches the expected prompt, it will stop
checking for any output from the process and return the expected prompt.
This solves the issue in the previous sprint where the BufferedReader object was not reading the output of the process.

***

### Relevant JUnit tests

#### sendTest.bat
This windows batch script is used to assist in the JUnit tests. It prompts the user for an input and waits for this input. Once an input has been received it will print a new line with
the same string that was used as an input. The script then closes.

#### test.bat
This windows batch script is used to assist in the JUnit tests. It prompts the user for an input and waits for this input. Once an input has been received it will print a new line with
the same string that was used as an input. It will then prompt the user again for an input, and the same will occur as the previous prompt. The script then closes.

The script also has the option to set a wait time before it prompts the user for input, it is currently set at one second. The script uses a local ping test
with a delay as this approach provided what was intended.

#### sendTest()
This test spawns a process of `sendTest.bat`. It has been extended from the previous sprint by using its own new test batch script.

#### expectTest()
This test spawns a process of `test.bat`. This test has been extended from the previous sprint so that it will now prompt the user for two inputs, one after the first input has been returned. The returned string from the first prompt is sent back to the process and then it will prompt the user for a second input, which is then sent back to the process. The process is then destroyed, if it has been destroyed successfully and the prompts match what is expected then it will then also return true.

#### excpectTestException()
This test spawns a process of `test.bat`. This test ensures that the TimeoutException is thrown if the process does not meet the
timeout requirements of the method `expect()`. The timeout for `expect()` in this test is set to 50 milliseconds, this is less than
the one second of the wait in test.bat. Therefore a TimeoutException should be thrown.
The test will pass if the TimeoutException has been thrown by `expect()` and has also been caught by the JUnit test.