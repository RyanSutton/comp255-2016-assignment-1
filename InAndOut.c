#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv){
	if(argc > 0){
		printf("%s", argv[1]);
		fflush(stdout);
		fclose(stdout);
		fclose(stderr);
		exit(0);
	}
	return 0;
}