:: set class path for windows

set CLASSPATH="$CLASSPATH;src/test/java/lib/*;src/main/java/;src/test/java/"

:: compile java classes

javac src/main/java/*.java src/test/java/*.java

:: run tests

java TestRunner

pause>nul