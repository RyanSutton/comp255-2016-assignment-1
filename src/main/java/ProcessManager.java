import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

/**
 * Create a running process and manage interaction with it
 */
public class ProcessManager {

	/**
	 * The strings for which the analyser is built
	 */
	String program;
	String[] arguments;

	ProcessBuilder processBuild;
	Process process;

	/**
	 * Make a running process with
	 *
	 * @param executable
	 *            The program to run
	 * @param args
	 *            The arguments of the program
	 */
	public ProcessManager(String executable, String[] args) {
		program = executable;
		arguments = args;
	}

	/**
	 * Spawn a process
	 */
	public void spawn() {
		ArrayList<String> processList = new ArrayList<String>();
		processList.add(program);
		for (String arg : arguments) {
			processList.add(arg);
		}
		try {
			processBuild = new ProcessBuilder(processList);
			process = processBuild.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Spawn a process and collect the results
	 * 
	 * @return the output of the process that was spawned
	 */
	public String spawnAndCollect() {
		ArrayList<String> processList = new ArrayList<String>();
		processList.add(program);
		for (String arg : arguments) {
			processList.add(arg);
		}
		try {
			processBuild = new ProcessBuilder(processList);
			process = processBuild.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

		InputStream spawnOutput = process.getInputStream();
		InputStreamReader outputReader = new InputStreamReader(spawnOutput);
		BufferedReader bufferedReader = new BufferedReader(outputReader);

		String inputLine;
		String completeInput = "";
		try {
			while ((inputLine = bufferedReader.readLine()) != null) {
				completeInput += (inputLine + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return completeInput;
	}

	/**
	 * Spawn a process and collect the results or throw an exception if no
	 * answer before the timeout
	 *
	 * @param timeout
	 *            The timeout in milliseconds
	 * @throws TimeoutException
	 */
	public String spawnAndCollectWithTimeout(int timeout) throws TimeoutException {
		ArrayList<String> processList = new ArrayList<String>();
		long startTime = 0;
		String inputLine;
		String completeInput = "";
		processList.add(program);
		for (String arg : arguments) {
			processList.add(arg);
		}
		try {
			processBuild = new ProcessBuilder(processList);
			process = processBuild.start();
			startTime = System.currentTimeMillis();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		while(process.isAlive()){
			InputStream spawnOutput = process.getInputStream();
			InputStreamReader outputReader = new InputStreamReader(spawnOutput);
			BufferedReader bufferedReader = new BufferedReader(outputReader);

			try {
				while ((inputLine = bufferedReader.readLine()) != null) {
					completeInput += (inputLine + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(System.currentTimeMillis()-startTime > timeout){
				throw new TimeoutException();
			}
		}
		return completeInput;
	}
	
	/**
	 * Send a string to the process
	 */
	public boolean send(String s){
		if(isRunning()){
			OutputStream outputStream = process.getOutputStream();
			BufferedWriter processWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
			try {
				processWriter.write(s);
				processWriter.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				try {
					processWriter.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return false;
			}
		}
		return false;
	}
	/**
	 * Send a string to the process and collect the result
	 * upto a 'prompt' or throw an
	 * exception if the prompt is not seen before the timeout
	 * 
	 * @param timeout The timeout in milliseconds
	 * @param prompt The expected prompt
	 * @throws IOException 
	 */	
	public String expect(Pattern prompt, int timeout) throws TimeoutException, IOException {
		long startTime = System.currentTimeMillis();
		long timeLimit = startTime + timeout;
		String currString;
        InputStream spawnOutput = process.getInputStream();
		InputStreamReader outputReader = new InputStreamReader(spawnOutput);
		BufferedReader bufferedReader = new BufferedReader(outputReader);
        try {
            while ((currString = bufferedReader.readLine()) != null && System.currentTimeMillis() < timeLimit){
                if(currString.equals(prompt.pattern())){
                    break;
                }
            }
            if (System.currentTimeMillis() >= timeLimit){
                throw new TimeoutException();
            }
            else if (prompt.pattern().equals(currString)){
                return  prompt.pattern();
            }          
        }
        catch (TimeoutException e){
            e.printStackTrace();
        }
        catch (IOException e){
        	e.printStackTrace();
        }
        return null;
	}

	/**
	 * Kill the process
	 */
	public void destroy() {
		process.destroy();
	}

	/**
	 * Checks to see if the process is running
	 * 
	 * @return true if the process is running, false if the process is not
	 *         running
	 */
	public boolean isRunning() {
		if (process.isAlive()) {
			return true;
		}
		return false;
	}
}
