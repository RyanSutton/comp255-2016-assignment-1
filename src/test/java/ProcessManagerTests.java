import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

/**
 * Test suite for Longest Common Subsequences
 */
public class ProcessManagerTests {

    /**
     * Test for spawning a process
     */
    @Test
    public void spawnTest() {
        String prog = "notepad.exe";
        String[] args = {"COMP255"};

        ProcessManager p = new ProcessManager(prog, args);

        p.spawn();
        assertTrue(p.isRunning());
    }
    
    /**
     * Test for destroying a test
     * Waits to ensure that the process is running
     */
    @Test
    public void destroyTest(){
    	String prog = "notepad.exe";
    	String[] args = {"COMP255"};
    	ProcessManager p = new ProcessManager(prog, args);
    	p.spawn();
    	try {
			Thread.sleep(1500);
		} 
    	catch (InterruptedException e) {
			e.printStackTrace();
		}
    	p.destroy();
        assertFalse(p.isRunning());
    }
    
	/**
	 * Test for spawning a process
	 * and then returning the output of that process
	 * NOTE: This JUnit test only works on windows machines
	 */
    @Test
    public void spawnAndCollectTest(){
    	String prog = "tasklist.exe";
    	String[] args = {"/?"};
    	ProcessManager p = new ProcessManager(prog, args);
    	assertFalse(p.spawnAndCollect() == null);
    	assertTrue(p.spawnAndCollect().contains("Description:") 
    			&& p.spawnAndCollect().contains("This tool displays a list of currently running processes on")
    			&& p.spawnAndCollect().contains("either a local or remote machine."));
    	args[0] = "";
    	ProcessManager p2 = new ProcessManager(prog, args);
    	assertFalse(p2.spawnAndCollect() == null);
    	assertTrue(p2.spawnAndCollect().contains("System Idle Process"));
    	prog = "InAndOut.exe";
    	args[0] = "hello, world!";
    	ProcessManager p3 = new ProcessManager(prog, args);
    	assertTrue(p3.spawnAndCollect().contains("hello, world!"));
    }
    
    /**
     * Test for spawning a process
     * and then returning the output of that process.
     * Will only pass the test if the spawned process
     * returns its output before the timeout
     * NOTE: This JUnit test only works on windows machines
     */
    @Test
    public void spawnAndCollectWithTimeoutTest() throws  TimeoutException{
    	//Waits two seconds before output is printed to stdout
    	String prog = "InAndOutWait.exe";
    	String[] args = {"hello"};
    	ProcessManager p = new ProcessManager(prog, args);
    	assertTrue(p.spawnAndCollectWithTimeout(2100).contains("hello"));
    }

    /**
     * Test for seeing if a TimeoutException is
     * thrown when the process spawned exceeds
     * the timeout before it returns its results.
     */
    @Test(expected = TimeoutException.class)
    public void spawnAndCollectWithTimeoutExceptionTest() throws TimeoutException{
    	String prog = "InAndOutWait.exe";
    	String[] args = {"hello"};
    	ProcessManager p = new ProcessManager(prog, args);
    	assertTrue(!p.spawnAndCollectWithTimeout(1000).contains("hello"));
    }
    /**
     * Test for sending a string to a process.
     * The test will pass if the string is successfully sent.
     * That occurs if the process is no longer running after receiving the string.
     */
    @Test
    public void sendTest(){
    	String prog = "sendTest.bat";
    	String[] args = {};
    	ProcessManager p = new ProcessManager(prog, args);
    	p.spawn();
    	p.send("hello");
    	if(!p.isRunning()){
    		assertTrue(true);
    	}
    }
    /**
     * Test for checking to see if a prompt from a process matches what is expected.
     * The test will pass if the prompt matches the expected prompt.
     * @throws TimeoutException
     */
    @Test
    public void expectTest() throws TimeoutException, IOException{
    	int timeout = 2000;
    	Pattern expectedPrompt = Pattern.compile("Enter text: ");
    	Pattern expectedPrompt2 = Pattern.compile("Enter text2: ");
    	String prog = "test.bat";
    	String[] args = {};
    	ProcessManager p = new ProcessManager(prog, args);
    	p.spawn();
    	assertTrue(p.expect(expectedPrompt, timeout).equals("Enter text: "));
    	p.send("hello");
    	assertTrue(p.expect(expectedPrompt2, timeout).equals("Enter text2: "));
    	p.destroy();
    	if(!p.isRunning()){
    		assertTrue(true);
    	}
    }
    
    @Test
    public void expectTestException() throws TimeoutException, IOException{
    	int timeout = 50;
    	Pattern expectedPrompt = Pattern.compile("Enter text: ");
    	String prog = "test.bat";
    	String[] args = {};
    	ProcessManager p = new ProcessManager(prog, args);
    	p.spawn();
    	try{
    		p.expect(expectedPrompt, timeout);
    	}
    	catch(TimeoutException e){
    		assertTrue(true);
    	}
    }
}
