Ryan Sutton - SID: 44609353
Liana Tames - SID: 44633254


This file should provide set up, requirements, and uage examples.

NOTE: At this point in time the JUnit tests will only work on a computer running Microsoft Windows.

Set up
======
You need a windows machine with a recent JDK and a C compiler (GCC).

Recent versions of junit and harmcrest and provided in  src/test/java/lib/

Moreover, the CLASSPATH should be set as follows:

~~~
> export CLASSPATH="$CLASSPATH:src/test/java/lib/*:src/main/java/:src/test/java/"
~~~

Compiling
=========
To compile the helper C executables:

~~~
> gcc -o InAndOut InAndOut.c
~~~
~~~
> gcc -o InAndOutWait InAndOutWait.c
~~~

To compile the Java program:

~~~
> javac src/main/java/*.java src/test/java/*.java
~~~

Running the tests
=================

~~~
> java TestRunner
~~~

The shell script "runall.sh" wil set the classpath, compile and run the tests.
